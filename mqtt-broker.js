"use strict";
const mongoPersistence = require('aedes-persistence-mongodb')
const aedes = require('aedes')({
    // /*
    persistence: mongoPersistence({
        url: 'mongodb://iot:internet@localhost:27017/admin',
        ttl: {
            packets: 300, // Number of seconds
            subscriptions: 300
        }
    })
    // */
});

aedes.on('subscribe', function(subscriptions, client) {
    console.log('MQTT client \x1b[32m' + (client ? client.id : client) +
        '\x1b[0m subscribed to topics: ' + subscriptions.map(s => s.topic).join('\n'), 'from broker', aedes.id)
});

aedes.on('unsubscribe', function(subscriptions, client) {
    console.log('MQTT client \x1b[32m' + (client ? client.id : client) +
        '\x1b[0m unsubscribed to topics: ' + subscriptions.join('\n'), 'from broker', aedes.id)
});

aedes.on('client', function(client) {
    console.log('Client Connected: \x1b[33m' + (client ? client.id : client) + '\x1b[0m', 'to broker', aedes.id)
});

aedes.on('clientDisconnect', function(client) {
    console.log('Client Disconnected: \x1b[31m' + (client ? client.id : client) + '\x1b[0m', 'to broker', aedes.id)
});

aedes.on('publish', async function(packet, client) {
    console.log('Client \x1b[31m' + (client ? client.id : 'BROKER_' + aedes.id) + '\x1b[0m has published', packet.payload.toString(), 'on', packet.topic, 'to broker', aedes.id)
});

const server = require('net').createServer(aedes.handle);
const httpServer = require('http').createServer()
const ws = require('websocket-stream')
ws.createServer({ server: httpServer }, aedes.handle)

let ip_address = process.env.IP || '0.0.0.0';
let port_mqtt = process.env.PORT || 1883;
let port_websocket = process.env.PORT || 8888;

server.listen(port_mqtt, ip_address, function() {
    console.debug('MQTT listening on port: ' + port_mqtt);
})

httpServer.listen(port_websocket, ip_address, function() {
    console.debug('MQTT-WS listening on port: ' + port_websocket)
});